//
//  Service.swift
//  FirstLearnPod
//
//  Created by Emrah Korkmaz on 10/23/19.
//  Copyright © 2019 Emrah Korkmaz. All rights reserved.
//

import Foundation


public class Service {
    
    
    private init () {}
    
    public static func doSomething() -> String {
        return "Did Some Stuff"
    }
    
    
}

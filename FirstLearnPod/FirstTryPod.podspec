Pod::Spec.new do |s|

  s.name         = "FirstTryPod"
  s.version      = "1.0.1"
  s.summary      = "My first poddy "
  s.description  = " asdasdas "
  s.homepage     = "https://gitlab.com/xcodewarrior/firstpod"
  s.license      = "MIT"
  s.author       = { "Emrah " => "emrahkrkmz1@gmail.com" }
  s.platform     = :ios, "12.0"
  s.source       = { :git => "https://gitlab.com/xcodewarrior/firstpod.git", :tag => "1.0.1" }
  s.source_files  = 'FirstTryPod/**/*'
  
end
